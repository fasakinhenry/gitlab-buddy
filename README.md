# GitLab Buddy

GitLab Buddy is a low-code/no-code solution integrated with AI helpers that accelerates the GitLab learning journey and simplifies the creation of pipelines.

This project is available on [https://gitlab-buddy.vercel.app](https://gitlab-buddy.vercel.app); however, if you would like to 
experiment locally, please feel free to do so with the forthcoming instructions.

## Project Description

GitLab Buddy reinvents the developer experience by removing barriers such as code errors and complexities in understanding GitLab CI/CD. Whether users are veterans or new to GitLab, the platform offers low-code/no-code selection tools for configuring pipeline elements such as images, rules, and jobs easily, supported by AI helpers that continuously adjust, fix, and explain code in real-time.

In line with the DevOps spirit of speed, efficiency, and collaboration, GitLab Buddy empowers developers of all levels to streamline workflows effortlessly. Its user-friendly interface and AI-driven tools accelerate processes and enhance the developer experience, ensuring easy navigation of the GitLab ecosystem.

GitLab Buddy aims to make GitLab's power understandable and accessible to all. As such, it has the potential to help millions of users. It can serve as a reliable companion, offering support and guidance at every stage of the DevOps journey to users at all levels of experience.

## Running GitLab Buddy locally

```bash
npm install
npm run dev
```
Please navigate to [http://localhost:3000](http://localhost:3000) in your browser to view the website.

## License

This project's software is licensed under the [MIT license](https://opensource.org/license/mit/). Please see the [LICENSE](LICENSE).

## Project Tools

- Next.js
- React.js
- Tailwind
- Vercel
- AWS Lambda
- OpenAI GPT API

## Text Description For Hackathon Requirement

### Problem Statement:

GitLab users, whether veterans or beginners, often encounter challenges while designing, deploying, and debugging their CI/CD pipelines due to GitLab's (sometimes) complex nature and error-prone coding. This issue is time-consuming, decreases productivity, and restricts users from completely leveraging GitLab's capabilities, creating a need for a solution that eases these challenges and accelerates pipeline crafting.

### Our Solution - GitLab Buddy:

GitLab Buddy is an innovative, low-code/no-code solution with intelligent AI helpers designed to effectively alleviate these problems. By clicking a series of buttons and typing out in modals, you can quickly select pipeline elements while the AI helpers continuously fix and explain the code in real-time.

**a) GitLab User Problems:** GitLab Buddy takes a multi-faceted approach to solve technical and learning problems of GitLab users. It caters to beginners by offering a simplified learning journey and also aids veterans by offering quick pipeline crafting and debugging features.

**b) Innovativeness:** GitLab Buddy is unique in transforming GitLab usage. Its innovative AI helpers not only fix the code but also adjust and explain it in real-time. It efficiently reduces error occurrence and time consumption, consequently enhancing the overall user experience.

**c) Quality:** With a modern, user-friendly, and responsive design made possible by Tailwind utility classes, GitLab Buddy offers excellent UX. It leverages technologies like React.js and Next.js to deliver a smooth, interactive interface that guides users through their workflows.

**d) Scalability:** GitLab Buddy is a fully serverless solution, built on reputable technologies including Vercel and AWS Lambda. This allows it to scale significantly in terms of user base, revenue, and operations without any loss in performance or quality. It also assures reliable, uninterrupted service with effective uptime.

**e) Total Addressable Market (TAM):** The opportunity for GitLab Buddy is massive. With millions of DevOps engineers worldwide struggling with pipeline designs and numerous beginners aspiring to learn, it has the potential to capture a substantial market.

**f) Feasibility:** GitLab Buddy is a practical solution leveraging readily available technology stack and an extremely cost-effective AI API. It enhances the learning curve, reduces time-to-market, and simplifies the GitLab ecosystem navigation. GitLab Buddy removes barriers to technical learning and debugging, making it a very valuable tool in the DevOps field.
