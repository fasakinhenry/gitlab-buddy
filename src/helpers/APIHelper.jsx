import React, { useState, useEffect, Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import clsx from 'clsx'
import Highlight, { defaultProps } from 'prism-react-renderer'
import axios from 'axios'
import { useCodeMirror } from '@/components/CodeMirrorContext';

const API_ENDPOINT =
  'https://whir5mpbde.execute-api.ap-southeast-2.amazonaws.com/prod/ai'

const YamlParser = ({ content }) => {

  const { code, updateCode } = useCodeMirror();

  const copyToClipboard = (code) => {
    navigator.clipboard.writeText(code)
      .then(() => {
        console.log('Code copied to clipboard successfully');
      })
      .catch((error) => {
        console.error('Failed to copy code to clipboard:', error);
      });
  };

  const copyToEditor = (code) => {
    updateCode(code);
  }

  const parseContent = (str) => {
    if (str.startsWith('```\n---')) {
      str = '```---' + str.substring(7);
    }
    const elements = []
    let buffer = ''
    let isCodeBlock = false
    let startPosition = 0

    for (let i = 0; i < str.length; i++) {
      if (
        !isCodeBlock &&
        (str.substring(i, i + 3) === '```' || str.startsWith('```yaml', i))
      ) {
        if (buffer) {
          // Handle buffer as normal text: parse formatting and new lines
          elements.push(...parseFormatting(buffer))
          buffer = ''
        }
        // Set the startPosition for the code block
        startPosition = i + (str.startsWith('```yaml', i) ? 8 : 3)
        isCodeBlock = true
        // Skip the marker
        i += str.startsWith('```yaml', i) ? 7 : 2
      } else if (isCodeBlock && str.substring(i, i + 3) === '```') {
        // Directly render the code block from startPosition to current index
        elements.push(renderCodeBlock(str.substring(startPosition, i)))
        isCodeBlock = false
        i += 2 // Skip the closing ```
      } else if (!isCodeBlock) {
        buffer += str[i]
      }
    }

    if (buffer && !isCodeBlock) {
      // Handle any remaining non-code text
      elements.push(...parseFormatting(buffer))
    }

    return elements
  }

  const parseMono = (text, parentIndex = 0) => {
    return text.split('`').map((segment, index) => {
      const key = `mono-${parentIndex}-${index}`
      if (index % 2 === 1) {
        return (
          <span
            className="bg-slate-200 font-mono text-indigo-500 dark:bg-slate-700 dark:text-indigo-200"
            key={key}
          >
            {segment}
          </span>
        )
      } else {
        // Wrap the segment that may contain significant white spaces with a span
        return (
          <span key={key} style={{ whiteSpace: 'pre-wrap' }}>
            {segment}
          </span>
        )
      }
    })
  }

  // Update in parseFormatting to include a parent index in the call to parseMono
  const parseFormatting = (text, parentIndex = 0) => {
    let parts = text.split('**').reduce((result, part, index) => {
      // Forward the parentIndex and current index to parseMono for key generation
      if (index % 2 === 1) {
        result.push(
          <strong key={`bold-${parentIndex}-${index}`}>
            {parseMono(part, `${parentIndex}-${index}`)}
          </strong>
        )
      } else {
        result.push(...parseMono(part, `${parentIndex}-${index}`))
      }
      return result
    }, [])

    return parts.flatMap((part, index) =>
      typeof part === 'string'
        ? part.split('\n').map((line, lineIndex, array) =>
            lineIndex < array.length - 1 ? (
              [
                <span
                  style={{ whiteSpace: 'pre-wrap' }}
                  key={`line-${parentIndex}-${index}-${lineIndex}`}
                >
                  {line}
                </span>,
                <br key={`br-${parentIndex}-${index}-${lineIndex}`} />,
              ]
            ) : (
              <span
                style={{ whiteSpace: 'pre-wrap' }}
                key={`line-${parentIndex}-${index}-${lineIndex}`}
              >
                {line}
              </span>
            )
          )
        : part
    )
  }

  const renderCodeBlock = (code) => {
    const codeLanguage = 'javascript' // Assuming language for demonstration

    return (
      <div className="relative">
        <div className="absolute inset-0 rounded-2xl bg-gradient-to-tr from-sky-300 via-sky-300/70 to-blue-300 opacity-10 blur-lg" />
        <div className="absolute inset-0 rounded-2xl bg-gradient-to-tr from-sky-300 via-sky-300/70 to-blue-300 opacity-10" />
        <div className="relative rounded-2xl bg-[#0A101F]/80 ring-1 ring-white/10 backdrop-blur">
          <div className="absolute -top-px left-20 right-11 h-px bg-gradient-to-r from-sky-300/0 via-sky-300/70 to-sky-300/0" />
          <div className="absolute -bottom-px left-11 right-20 h-px bg-gradient-to-r from-blue-400/0 via-blue-400 to-blue-400/0" />
          <div className="mt-4 pl-4 pt-0.5">
            <button
              type="submit"
              id="submitButton"
              className="ml-2 mt-3 mb-0 inline-flex justify-center rounded-md border border-transparent bg-cyan-700 px-2 py-1 text-sm font-semibold text-white shadow-sm hover:bg-cyan-900 focus:outline-none focus:ring-2 focus:ring-cyan-500 focus:ring-offset-2"
              onClick={()=>copyToEditor(code)}
            >
              copy to editor
            </button>
            <button
              type="submit"
              id="submitButton"
              className="ml-2 mt-3 mb-0 inline-flex justify-center rounded-md border border-transparent bg-sky-700 px-2 py-1 text-sm font-semibold text-white shadow-sm hover:bg-sky-900 focus:outline-none focus:ring-2 focus:ring-sky-500 focus:ring-offset-2"
              onClick={()=>copyToClipboard(code)}
            >
              copy to clipboard
            </button>
            <div className="mt-3 flex items-start px-1 text-sm">
              <div
                aria-hidden="true"
                className="select-none border-r border-slate-300/5 pr-4 font-mono text-slate-400 dark:text-slate-600"
              >
                {Array.from({
                  length: code.split('\n').length,
                }).map((_, index) => (
                  <Fragment key={index}>
                    {(index + 1).toString().padStart(2, '0')}
                    <br />
                  </Fragment>
                ))}
              </div>
              <Highlight
                {...defaultProps}
                code={code}
                language={codeLanguage}
                theme={undefined}
              >
                {({
                  className,
                  style,
                  tokens,
                  getLineProps,
                  getTokenProps,
                }) => (
                  <pre
                    className={clsx(className, 'flex overflow-x-auto pb-6')}
                    style={style}
                  >
                    <code className="px-4">
                      {tokens.map((line, lineIndex) => (
                        <div key={lineIndex} {...getLineProps({ line })}>
                          {line.map((token, tokenIndex) => (
                            <span
                              key={tokenIndex}
                              {...getTokenProps({ token })}
                            />
                          ))}
                        </div>
                      ))}
                    </code>
                  </pre>
                )}
              </Highlight>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const [parsedContent, setParsedContent] = useState([])

  useEffect(() => {
    if (content) {
      const components = parseContent(content)
      setParsedContent(components)
    }
  }, [content])

  return (
    <div className="text-indigo-800 dark:text-indigo-300">{parsedContent}</div>
  )
}

// Function to send a POST request with the given string input
export default async function APIHelper(stringInput) {
  try {
    const response = await axios.post(
      API_ENDPOINT,
      {
        body: `${stringInput}.`,
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )

    // Call YamlParser with the response data
    return <YamlParser content={response.data.body.content} />
  } catch (error) {
    console.error('Error:', error)
    throw new Error('Failed to send POST request.')
  }
}
