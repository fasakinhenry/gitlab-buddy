import yaml from 'js-yaml';

// Convert YAML to array while preserving order
export function yamlToArray(yamlText) {
  try {
    const obj = yaml.load(yamlText);
    const array = [];
    for (const key of Object.keys(obj)) {
      array.push([key, obj[key]]);
    }
    return array;
  } catch (error) {
    // console.error('Error parsing YAML:', error);
    // Return an empty array or handle the error as needed
    return [];
  }
}

// Utility function to order YAML blocks
function orderYamlBlocks(blocksOrder, array) {

  return array.sort((a, b) => {
    const indexA = blocksOrder.indexOf(a[0]);
    const indexB = blocksOrder.indexOf(b[0]);
    return indexA - indexB;
  });
}

// Convert array to YAML with proper formatting and order
export function arrayToYaml(array) {
  // Define the desired order of blocks
  const blocksOrder = ['workflow', 'image', 'variables', 'stages'];

  // Order the array based on the desired order
  const orderedArray = orderYamlBlocks(blocksOrder, array);

  // Convert the ordered array to YAML
  let yamlString = '';

  // Manually iterate over the ordered array based on blocksOrder
  blocksOrder.forEach(block => {
    const entry = orderedArray.find(([key]) => key === block);
    if (entry) {
      const [key, value] = entry;
      if (key === 'workflow') {
        // Skip workflow if it's not present
        if (!value) return;
        // Add workflow block
        yamlString += `${key}:\n`;
        yamlString += "  rules:\n"
        value.rules.forEach(rule => {
          if (typeof rule === 'object') {
          }
          if (typeof rule === 'object') {
            Object.entries(rule).forEach(([ruleKey, ruleValue]) => {
                  yamlString += `    - ${ruleKey}: '${ruleValue}'\n`;
                });
          } else {
            yamlString += `    - ${rule}\n`;
          }
        });
      } else if (key === 'image' || key === 'variables') {
        // Skip image and variables if they're not present
        if (!value) return;
        if (key === 'variables') {
          // Format variables block as key-value pairs
          yamlString += `${key}:\n`;
          Object.entries(value).forEach(([variableKey, variableValue]) => {
            yamlString += `  ${variableKey}: "${variableValue}"\n`; // Wrap values in double quotes
          });
        } else {
          yamlString += `${key}: "${value}"\n`; // Wrap image values in double quotes
        }
      } else if (key === 'stages') {
        // Skip stages if it's not present
        if (!value) return;
        // Add stages block
        yamlString += `${key}:\n`;
        value.forEach(stage => {
          yamlString += `  - ${stage}\n`;
        });
      }
      if (!yamlString.endsWith('\n\n')) { // Add a newline if not present
        yamlString += '\n'; 
      }
    }});

  // Add jobs as separate blocks for keys not included in blocksOrder
  const jobsArray = orderedArray.filter(([key]) => !blocksOrder.includes(key));
  jobsArray.forEach(([jobKey, jobValue], index, array) => {
    yamlString += `${jobKey}:\n`;
    Object.entries(jobValue).forEach(([prop, propValue]) => {
      if (prop === 'script' || prop === 'rules') {
        yamlString += `  ${prop}:\n`; // Add "script" keyword
        propValue.forEach(script => {
          yamlString += `    - ${script}\n`; // Indent scripts
        });
      } else {
        yamlString += `  ${prop}: ${propValue}\n`;
      }
    });
    if (index !== array.length - 1 || !yamlString.endsWith('\n\n')) { // Add a space between jobs if not the last one or if no double newline is present
      yamlString += '\n'; 
    }
  });

  // Ensure there is exactly one space at the end of the file
  yamlString = yamlString.replace(/\s+$/, '\n');

  // Ensure the start and end with '---' and a blank line
  console.log("output:\n",`---\n${yamlString}`)
  return `---\n${yamlString}`;
}


// Set a workflow rule
export function setWorkflowRule(array, rule) {
  console.log(array)
  let workflowIndex = array.findIndex(([key]) => key === 'workflow');
  if (workflowIndex === -1) {
    workflowIndex = array.length;
    array.push(['workflow', { rules: [rule] }]);
  } else {
    const workflow = array[workflowIndex][1];
    if (!workflow.rules) {
      workflow.rules = [rule];
    } else {
      workflow.rules.push(rule);
      console.log(workflow.rules)
    }
    array[workflowIndex][1] = workflow;
  }
}

export function setJobWorkflowRule(array, jobName, rule) {
  // Find the job index
  const jobIndex = array.findIndex(([key]) => key === jobName);
  if(jobIndex == -1) {
    console.error("Job not found at index")
  }
  array[jobIndex][1].rules = [rule];
  
}

// Set the image
export function setImage(array, image) {
  let imageIndex = array.findIndex(([key]) => key === 'image');
  if (imageIndex === -1) {
    imageIndex = array.length;
    array.push(['image', image]);
  } else {
    array[imageIndex][1] = image;
  }
}

// Add a stage at the specified index
export function addStage(array, stageName, index) {
  let stagesIndex = array.findIndex(([key]) => key === 'stages');
  if (stagesIndex === -1) {
    stagesIndex = array.length;
    array.push(['stages', [stageName]]);
  } else {
    const stages = array[stagesIndex][1];
    stages.splice(index, 0, stageName);
    array[stagesIndex][1] = stages;
  }
}

// Add a job to the specified stage
export function addJobToStage(array, stageIndex, newJobName) {
  // Find the index of the stage in the array
  const stageKey = "stages";
  const stageEntryIndex = array.findIndex(([key]) => key === stageKey);
  if (stageEntryIndex === -1 || !Array.isArray(array[stageEntryIndex][1])) {
    console.error("Stages not found or invalid format");
    return;
  }

  const stages = array[stageEntryIndex][1];
  if (stageIndex < 0 || stageIndex >= stages.length) {
    console.error("Invalid stage index");
    return;
  }

  // Create a new job entry
  const newJobEntry = [
    newJobName,
    {
      stage: stages[stageIndex],
      script: []
    }
  ];

  // Insert the new job entry after the last job in the specified stage
  const stageJobs = array.filter(([key]) => key !== stageKey && key.endsWith("_job") && array.find(([k]) => k === key)[1].stage === stages[stageIndex]);
  const lastJobIndex = stageJobs.length > 0 ? array.indexOf(stageJobs[stageJobs.length - 1]) : array.indexOf(array[stageEntryIndex]);
  array.splice(lastJobIndex + 1, 0, newJobEntry);
}



// Add a script to the specified job
export function addScriptToJob(array, stageIndex, jobName, script) {
  // Find the job in the array
  const jobIndex = array.findIndex(([key]) => key === jobName);
  if (jobIndex === -1) {
    console.error("Job not found");
    return;
  }

  // Check if the job is under the specified stage
  const stageKey = "stages";
  const stageEntryIndex = array.findIndex(([key]) => key === stageKey);
  if (stageEntryIndex === -1 || !Array.isArray(array[stageEntryIndex][1])) {
    console.error("Stages not found or invalid format");
    return;
  }

  const stages = array[stageEntryIndex][1];
  if (stageIndex < 0 || stageIndex >= stages.length || stages[stageIndex] !== array[jobIndex][1].stage) {
    console.error("Job is not under the specified stage");
    return;
  }

  // Add the script to the job
  array[jobIndex][1].script.push(script);
}

export function addVariable(array, name, value) {
  let variablesIndex = array.findIndex(([key]) => key === 'variables');
  if (variablesIndex === -1) {
    variablesIndex = array.length;
    array.push(['variables', { [name]: value }]);
  } else {
    const variables = array[variablesIndex][1];
    variables[name] = value;
    array[variablesIndex][1] = variables;
  }
}