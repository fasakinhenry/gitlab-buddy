---
title: YAML Formatting and Rules
description: Learn how to format YAML and understand its basic rules and conventions.
---

YAML may seem tricky at first, but once you get the hang of it, it's like writing a simple recipe for your computer to follow!

## Formatting YAML

YAML stands for "Yet Another Markup Language," and it's designed to be easy for both humans and computers to read. Here are some basic formatting rules:

- **Indentation**: Use spaces (not tabs!) to indent your YAML file. Each level of indentation represents a hierarchy.

- **Key-Value Pairs**: Use a colon (**:**) to separate keys and values. For example: 
  ```yaml
  name: John

- **Lists**: Use hyphens (**-**) followed by a space to denote items in a list. For example:
  ```yaml
  fruits:
    - apple
    - banana
    - orange

- **Comments**: You can add comments in YAML using the hash symbol (#). Comments are ignored by the parser and are for humans to read.
  
## Rules of YAML

YAML has some rules you need to follow to avoid confusion:

- **Consistent Indentation**: Make sure to use the same number of spaces for each level of indentation throughout your YAML file. Mixing spaces and tabs can cause errors!

- **Quoting Strings**: If your string value contains special characters like colons or starts with characters that might be misinterpreted (like true, false, or null), it's a good idea to enclose it in quotes (**"** or **'**).

- **YAML is Case Sensitive**: Be careful with capitalization! Name and name are treated as different keys in YAML.

- **Whitespace Matters**: YAML is sensitive to whitespace, so be sure to include spaces where necessary, especially around colons and hyphens.

- **No Tabs**: YAML doesn't allow tabs for indentation. Stick to spaces!

## Practice Makes Perfect
Don't worry if YAML feels a bit confusing at first. With a bit of practice, you'll get the hang of it! Start by writing simple YAML files for your projects, and soon you'll be a YAML pro!

Remember, YAML is all about making your data easy to read and understand, both for you and your computer. So take your time, keep it tidy, and happy coding!
