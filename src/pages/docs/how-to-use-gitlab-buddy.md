---
title: How to Use GitLab Buddy
description: Learn how to leverage GitLab Buddy's powerful features for streamlining your CI/CD pipeline crafting experience.
---

In this guide, we'll walk you through the key features of GitLab Buddy and how to make the most out of them.

## AI-Enabled Coding Assistance

GitLab Buddy comes with intelligent AI helpers that make coding a breeze. Here's what you can do with AI:

- **Adjust with AI**: Let AI optimize your code for efficiency and best practices.
- **Explain with AI**: Get real-time explanations from AI on why certain changes were made, helping you learn and understand GitLab better.
  
![AI Components](https://gitlab.com/zkpservices1/gitlab-buddy/-/raw/main/src/images/ai-components.png)

- **Fix with AI**: If you encounter any syntax errors, AI can automatically fix them for you.

![Fix with AI](https://gitlab.com/zkpservices1/gitlab-buddy/-/raw/main/src/images/fix-with-ai.png)
  
## Pre-built Components via Simple Modals

GitLab Buddy simplifies the process of adding essential components to your CI/CD pipeline. With just a few clicks in simple modals, you can add:

- **Images**: Easily integrate images into your pipeline configurations.
- **Variables**: Define variables to be used across your pipeline stages and jobs.
- **Stages**: Organize your pipeline into stages for better management and control.
- **Jobs**: Specify individual tasks or jobs to be executed within each stage.
- **Workflow Rules**: Set up rules to control the flow and execution of your pipeline based on specific conditions.

![Component Modals](https://gitlab.com/zkpservices1/gitlab-buddy/-/raw/main/src/images/component-modals.png)

## Download File

Once you've crafted your CI/CD pipeline using GitLab Buddy, you can easily download the configuration file. This allows you to keep a local copy for backup or further customization as needed.

## Copy to Clipboard

Need to share your pipeline configuration with team members or collaborators? GitLab Buddy lets you copy the generated configuration to your clipboard with just a click. Simply paste it into your GitLab project and you're good to go!

With these powerful features, GitLab Buddy transforms the way you work with GitLab, making pipeline crafting faster, easier, and more efficient than ever before. Let's get started and unleash the full potential of GitLab Buddy!
