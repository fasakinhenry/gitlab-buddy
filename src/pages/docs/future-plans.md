---
title: Future Plans
description: Read about our future plans for GitLab Buddy.
---

GitLab Buddy is a constantly evolving prototype, and there are many new exciting features we are looking to add in the near future.

## Direct GitLab Repository Integration

We are planning on adding the ability to directly integrate with your public or private GitLab repository. This would allow you to directly commit your `gitlab-ci.yml` changes to your repository - no need to copy and paste manually!

## Pipeline Graph Visualization

This is one of our most exciting future features in the works. We want to add a new tab to the GitLab Buddy app that will transform your current pipeline code into a beautiful and comprehensible flow graph visualization. 

![Graph Visualization](https://gitlab.com/zkpservices1/gitlab-buddy/-/raw/main/src/images/graph.png)
  
## Further component support

Here are some of the further components we would like to support with our low/no-code modals:

- More complex workflow rules
- **before_script** component
- **after_script** component
- **services** component
- **artifacts** components
- **cache** component
- **dependencies** components