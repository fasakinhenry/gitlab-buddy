import React, { useEffect, useState } from 'react'
import { QuickLinks, QuickLink } from '@/components/QuickLinks'
import { CodeMirror } from '@/components/CodeMirror'
import { useCodeMirror } from '@/components/CodeMirrorContext'
import { AddImageModal } from '@/components/AddImageModal'
import { AddStageModal } from '@/components/AddStageModal'
import { AddVariablesModal } from '@/components/AddVariablesModal'
import { AddJobModal } from '@/components/AddJobModal'
import { FixWithAIModal } from '@/components/FixWithAIModal'
import { Fragment } from 'react'
import YAML from 'js-yaml'
import { AddWorkflowRulesModal } from '@/components/AddWorkflowRulesModal'

const GitLabBuddy = () => {
  useEffect(() => {
    document.title = 'GitLab Buddy'
    const metaDescription = document.querySelector('meta[name="description"]')
    if (metaDescription) {
      metaDescription.setAttribute(
        'content',
        'GitLab Buddy is a low-code/no-code solution with AI helpers that will help you both learn how to use GitLab and craft, adjust, and debug awesome pipelines right away.'
      )
    }
  }, [])

  const { code, updateCode, error } = useCodeMirror()
  const [selectedComponent, setSelectedComponent] = useState(null)

  const openWorkflowRulesModal = () => {
    setSelectedComponent('workflow-rules')
  }
  const openVariablesModal = () => {
    setSelectedComponent('variables')
  }
  const openImagesModal = () => {
    setSelectedComponent('images')
  }
  const openStagesModal = () => {
    setSelectedComponent('stages')
  }
  const openJobsModal = () => {
    setSelectedComponent('jobs')
  }
  const openFixWithAIModal = () => {
    setSelectedComponent('fix-ai')
  }

  return (
    <>
      <h1 className="font-bold" id="gitlab-buddy">
        GitLab Buddy
      </h1>

      {error && (
        <div
          className="mb-2 relative rounded border border-red-400 bg-red-900 px-4 py-3 font-mono text-white"
          role="alert"
        >
          <strong className="font-bold text-white">
            There's a problem with your YAML code:
          </strong>
          <br />
          <br />
          {error.split('\n').map((line, index) => (
            <Fragment key={index}>
              <span>{line}</span>
              <br />
            </Fragment>
          ))}
          <br />
          <strong className="font-bold text-white">Fix with AI below.</strong>
        </div>
      )}
      <CodeMirror code={code} updateCode={updateCode} />
      {!error ? (
        <>
          <h3 className="mt-12">Add Components to Your <span className="font-mono">.gitlab-ci.yml</span> File</h3>
          <p>Use our tools below to add new components to your pipeline.</p>
          <p>
            <span className="lead"></span>
          </p>
        </>
      ) : (
        <>
          <h3 className="mt-12">You can't add new components right now.</h3>
          <p>
            There is an error in your YAML code, so adding new components is
            disabled until the error is fixed.
          </p>
          <p>Use the tool below to fix your code with AI.</p>
          <p>
            <span className="lead"></span>
          </p>
        </>
      )}
      <QuickLinks>
        {!error ? (
          <>
            <QuickLink
              title="Image"
              icon="plugins"
              href="/"
              onClick={openImagesModal}
              description="Set an image for your pipeline to use."
            />
            <QuickLink
              title="Workflow Rules"
              icon="installation"
              href="/"
              onClick={openWorkflowRulesModal}
              description="Set up rules controlling when and how your pipeline runs."
            />
            <QuickLink
              title="Variables"
              icon="variables"
              href="/"
              onClick={openVariablesModal}
              description="Set up variables to use anywhere in your pipeline."
            />
            <QuickLink
              title="Stages"
              icon="theming"
              href="/"
              onClick={openStagesModal}
              description="Add a new stage to your pipeline."
            />
            <QuickLink
              title="Jobs"
              icon="presets"
              href="/"
              onClick={openJobsModal}
              description="Add a new job to your pipeline."
            />
          </>
        ) : (
          <QuickLink
            title="Fix with AI"
            icon="warning"
            href="/"
            onClick={openFixWithAIModal}
            description="You won't be able to add any components using our tools until your YAML file is valid."
          />
        )}
      </QuickLinks>

      <AddImageModal
        open={selectedComponent === 'images'}
        onClose={(error) => {
          setSelectedComponent(null)
        }}
      />

      <AddVariablesModal
        open={selectedComponent === 'variables'}
        yamlData={code}
        onClose={(error) => {
          setSelectedComponent(null)
        }}
      />

      <AddJobModal
        open={selectedComponent === 'jobs'}
        yamlData={code}
        onClose={(error) => {
          setSelectedComponent(null)
        }}
      />

      <AddStageModal
        open={selectedComponent === 'stages'}
        props=""
        yamlData={code}
        options=""
        onClose={(error) => {
          setSelectedComponent(null)
        }}
      />
      <AddWorkflowRulesModal
        open={selectedComponent === 'workflow-rules'}
        props=""
        yamlData={code}
        options=""
        onClose={(error) => {
          setSelectedComponent(null)
        }}
      />
      <FixWithAIModal
        open={selectedComponent === 'fix-ai'}
        props=""
        yamlData={code}
        options=""
        onClose={(error) => {
          setSelectedComponent(null)
        }}
      />

      <p>
        More components coming soon.
      </p>
    </>
  )
}

export default GitLabBuddy
