import React, { createContext, useState, useContext, useEffect } from 'react'
import YAML from 'js-yaml'

// Create a context for CodeMirror state
const CodeMirrorContext = createContext()

// Provider component
export const CodeMirrorProvider = ({ children }) => {
  const [code, setCode] = useState(`---
stages:
  - build
  - test
  - deploy

build_job:
  stage: build
  script:
    - echo "Building..."

test_job:
  stage: test
  script:
    - echo "Testing..."

deploy_job:
  stage: deploy
  script:
    - echo "Deploying..."
`) // Initial code for CodeMirror

  const [error, setError] = useState(null)

  // Function to check YAML validity
  const parseYaml = (yamlText) => {
    try {
      console.log("context", yamlText)
      YAML.load(yamlText)
      setError(null) // Clear error if YAML is valid
    } catch (error) {
      console.log(error.message)
      setError(error.message) // Set error message with newline characters
    }
  }

  // Function to update code
  const updateCode = (newCode) => {
    setCode(newCode)
  }

  useEffect(() => {
    updateCode(code) // Update the code immediately
    parseYaml(code)
  }, [code, updateCode]) // Include updateCode in the dependency array

  return (
    <CodeMirrorContext.Provider value={{ code, updateCode, error }}>
      {children}
    </CodeMirrorContext.Provider>
  )
}

// Custom hook to consume the context
export const useCodeMirror = () => useContext(CodeMirrorContext)
