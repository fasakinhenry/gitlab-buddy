import { Fragment, useEffect, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { BarsArrowDownIcon, XMarkIcon } from '@heroicons/react/24/outline'
import {
  yamlToArray,
  arrayToYaml,
  addScriptToJob,
  addJobToStage,
} from '@/helpers/Converters'
import { yaml } from '@codemirror/lang-yaml'
import { useCodeMirror } from '@/components/CodeMirrorContext'

export function AddJobModal({ open, onClose, yamlData }) {
  const { code, updateCode } = useCodeMirror()
  const [parsedYaml, setParsedYaml] = useState(yamlToArray(code))
  const [newJobName, setNewJobName] = useState('NEW_JOB')
  const [stages, setStages] = useState([])
  const [newJobScript, setNewJobScript] = useState('')
  const [selectedStageIndex, setSelectedStageIndex] = useState(0)

  const presetScripts = [
    {
      title: 'Echo a Variable',
      script: 'echo ${VARIABLE_NAME}',
    },
    {
      title: 'Run Unit Tests',
      script: 'npm install\nnpm test',
    },
    {
      title: 'Build Docker Image',
      script: 'docker build -t myapp .',
    },
    {
      title: 'Deploy to Staging',
      script:
        "ssh user@staging-server 'docker pull myapp:latest && docker run -d --name myapp-container -p 8080:8080 myapp:latest'",
    },
  ]

  useEffect(() => {
    // Parse the YAML data and extract the stages
    const parsedYamlData = yamlToArray(yamlData)
    const stagesArray = parsedYamlData.find((entry) => entry[0] === 'stages')
    const stagesData = stagesArray ? stagesArray[1] : []
    const points = [
      ...stagesData.map((stage, index) => ({
        label: `In ${stage} stage`,
        index: index,
      })),
    ]
    setStages(points)
    // Set parsed YAML data state
    setParsedYaml(parsedYamlData)
  }, [yamlData])

  const handleJobNameChange = (event) => {
    setNewJobName(event.target.value)
  }

  const handleScriptChange = (event) => {
    setNewJobScript(event.target.value)
  }

  const handleSubmit = () => {
    console.log(`newJobName: ${newJobName}`)
    console.log(`selectedStageIndex: ${selectedStageIndex}`)
    console.log(`newJobScript: ${newJobScript}`)
    addJobToStage(parsedYaml, selectedStageIndex, newJobName)
    addScriptToJob(parsedYaml, selectedStageIndex, newJobName, newJobScript)
    console.log(parsedYaml)
    const parsedYamlData = arrayToYaml(parsedYaml)
    updateCode(parsedYamlData)
    onClose()
  }

  const handleSelectChange = (event) => {
    setSelectedStageIndex(event.target.value)
  }

  const dropdownOptions = stages.map((point, index) => (
    <option key={index} value={point.index}>
      {point.label}
    </option>
  ))

  const calculateRows = (text) => {
    const lineCount = (text.match(/\n/g) || []).length + 1
    return Math.min(lineCount + 1, 10) // Limit to maximum of 10 rows
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 z-50 overflow-y-auto dark:bg-opacity-75"
        onClose={onClose}
      >
        <div className="flex min-h-screen items-center justify-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-600 bg-opacity-50 backdrop-blur-sm backdrop-filter dark:bg-gray-900 dark:bg-opacity-50" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div>
              <div className="relative mx-auto mt-6 max-w-screen-2xl rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl dark:bg-gray-800 sm:my-20 sm:w-full sm:max-w-3xl sm:p-6">
                <div className="absolute right-0 top-0 hidden pr-4 pt-4 sm:block">
                  <button
                    type="button"
                    className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:bg-gray-800 dark:text-gray-600 dark:hover:text-gray-400"
                    onClick={onClose}
                  >
                    <span className="sr-only">Close</span>
                    <XMarkIcon
                      className="h-6 w-6 text-indigo-500 hover:text-indigo-600 dark:text-indigo-300 dark:hover:text-indigo-400"
                      aria-hidden="true"
                    />
                  </button>
                </div>
                <Dialog.Title
                  as="h3"
                  className="text-lg font-bold text-gray-900 dark:text-white"
                >
                  <div>
                    {/* <BarsArrowDownIcon
                      className="h-6 w-6 text-indigo-600 dark:text-indigo-400"
                      aria-hidden="true"
                  /> */}
                  </div>
                  Jobs
                </Dialog.Title>
                <hr className="my-4 border-gray-300 dark:border-gray-700" />
                <h2 className="text-med font-semibold text-gray-900 dark:text-white">
                  ➕ Add a job
                </h2>
                <div className="mt-2 max-h-[40vh] min-w-[16rem] overflow-y-auto px-1 pb-1 md:min-w-[40rem] lg:max-h-[65vh] lg:min-w-[40rem]">
                  <div className="mt-4">
                    <label
                      htmlFor="requestID"
                      className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Job name:
                    </label>
                    <textarea
                      id="newJobName"
                      name="newJobName"
                      className="relative mt-1 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                      rows={1}
                      spellCheck="false"
                      value={newJobName}
                      onChange={handleJobNameChange}
                    />
                  </div>
                  <div className="mt-4">
                    <label
                      htmlFor="requestID"
                      className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Which stage should this job be under?
                    </label>
                    <select
                      id="stageLocation"
                      className="relative mt-1 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                      rows={1}
                      readOnly
                      spellCheck="false"
                      onChange={handleSelectChange}
                    >
                      {dropdownOptions}
                    </select>
                  </div>
                  <div className="mt-4">
                    <label
                      htmlFor="requestID"
                      className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Enter a script for the job:
                    </label>
                    <textarea
                      id="dynamicTextarea"
                      className="relative mt-1 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                      rows={calculateRows(newJobScript)} // Set the number of rows dynamically
                      spellCheck="false"
                      value={newJobScript}
                      onChange={handleScriptChange}
                    />
                  </div>
                  <div className="mt-2">
                    <label
                      htmlFor="presetScripts"
                      className="block pt-2 pb-2 text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Preset Scripts:
                    </label>
                    <div className="grid grid-cols-2 gap-2">
                      {presetScripts.map((preset, index) => (
                        <button
                          key={index}
                          type="button"
                          className="inline-flex w-full justify-center rounded-md bg-blue-600 py-1 text-sm text-white hover:bg-blue-800 focus:outline-none focus:ring-2 focus:ring-blue-800 focus:ring-offset-2"
                          onClick={() => setNewJobScript(preset.script)}
                        >
                          {preset.title}
                        </button>
                      ))}
                    </div>
                  </div>
                  <hr className="my-4 border-gray-300 dark:border-gray-700" />
                </div>

                <div className="sm:flex sm:flex-row-reverse">
                  <button
                    type="submit"
                    id="submitButton"
                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-4 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    onClick={handleSubmit}
                  >
                    Add Job
                  </button>
                  <button
                    type="button"
                    className="ml-3 mt-3 inline-flex w-full justify-center rounded-md bg-slate-100 px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-slate-200 dark:bg-slate-800 dark:text-white dark:ring-gray-600 dark:hover:bg-slate-900 sm:mt-0 sm:w-auto"
                    onClick={onClose}
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
