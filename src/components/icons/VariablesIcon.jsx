import { DarkMode, Gradient, LightMode } from '@/components/Icon'

export function VariablesIcon({ id, color }) {
  return (
    <>
      <defs>
        <Gradient
          id={`${id}-gradient-dark-text`}
          color={color}
          gradientTransform="matrix(0 14 -14 0 16 20)"
        />
      </defs>
      <text x={0} y={20} fontSize="18" fill={`url(#${id}-gradient-dark-text)`} className="dark-mode-text font-semibold">{`{ 𝑥 }`}</text>
    </>
  )
}
