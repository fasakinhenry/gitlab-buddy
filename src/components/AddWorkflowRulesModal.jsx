import { Fragment, useEffect, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { BarsArrowDownIcon, XMarkIcon } from '@heroicons/react/24/outline'
import {
  yamlToArray,
  arrayToYaml,
  setWorkflowRule,
  setJobWorkflowRule,
} from '@/helpers/Converters'
import { yaml } from '@codemirror/lang-yaml'
import { useCodeMirror } from '@/components/CodeMirrorContext'

export function AddWorkflowRulesModal({ open, onClose, yamlData }) {
  const { code, updateCode } = useCodeMirror()
  const [parsedYaml, setParsedYaml] = useState(yamlToArray(code))
  const [newWorkflow, setNewWorkflow] = useState('')
  const [stages, setStages] = useState([])
  const [jobs, setJobs] = useState([])
  const [selectedStageIndex, setSelectedStageIndex] = useState(0)
  const [ruleLevel, setRuleLevel] = useState('pipeline')
  const [selectedJob, setSelectedJob] = useState('')

  const presetRules = [
    {
      title: 'Run when the trigger is merge request',
      script: 'if: \'$CI_PIPELINE_SOURCE == "merge_request_event"\'',
    },
    {
      title: 'Run when the trigger is a push',
      script: 'if: \'$CI_PIPELINE_SOURCE == "push"\'',
    },
    {
      title: 'Run when there is a merge request',
      script: "if: '$CI_MERGE_REQUEST_ID'",
    },
    {
      title: 'Run when there is a commit',
      script: 'if: $CI_COMMIT_BRANCH',
    },
  ]

  useEffect(() => {
    // Parse the YAML data and extract the stages
    const parsedYamlData = yamlToArray(yamlData)
    const stagesArray = parsedYamlData.find((entry) => entry[0] === 'stages')
    const stagesData = stagesArray ? stagesArray[1] : []
    const jobNames = parsedYamlData
      .filter(
        (entry) => typeof entry[1] === 'object' && entry[0].endsWith('_job')
      )
      .map((entry) => entry[0])

    // Set job names state
    setJobs(jobNames)
    // Set parsed YAML data state
    setParsedYaml(parsedYamlData)
  }, [yamlData])

  const handleWorkflowChange = (event) => {
    setNewWorkflow(event.target.value)
  }

  const handleSubmit = () => {
    if (ruleLevel == 'pipeline') {
      setWorkflowRule(parsedYaml, newWorkflow)
    } else if (ruleLevel == 'job') {
      setJobWorkflowRule(parsedYaml, selectedJob, newWorkflow)
    }

    // addScriptToJob(parsedYaml, selectedStageIndex, newJobName, newJobScript)
    const parsedYamlData = arrayToYaml(parsedYaml)
    updateCode(parsedYamlData)
    onClose()
  }

  const handleSelectChange = (event) => {
    setSelectedStageIndex(event.target.value)
  }

  const dropdownOptions = stages.map((point, index) => (
    <option key={index} value={point.index}>
      {point.label}
    </option>
  ))

  const calculateRows = (text) => {
    const lineCount = (text.match(/\n/g) || []).length + 1
    return Math.min(lineCount, 10) // Limit to maximum of 10 rows
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 z-50 overflow-y-auto dark:bg-opacity-75"
        onClose={onClose}
      >
        <div className="flex min-h-screen items-center justify-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-600 bg-opacity-50 backdrop-blur-sm backdrop-filter dark:bg-gray-900 dark:bg-opacity-50" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div>
              <div className="relative mx-auto mt-6 max-w-screen-2xl rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl dark:bg-gray-800 sm:my-20 sm:w-full sm:max-w-3xl sm:p-6">
                <div className="absolute right-0 top-0 hidden pr-4 pt-4 sm:block">
                  <button
                    type="button"
                    className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:bg-gray-800 dark:text-gray-600 dark:hover:text-gray-400"
                    onClick={onClose}
                  >
                    <span className="sr-only">Close</span>
                    <XMarkIcon
                      className="h-6 w-6 text-indigo-500 hover:text-indigo-600 dark:text-indigo-300 dark:hover:text-indigo-400"
                      aria-hidden="true"
                    />
                  </button>
                </div>
                <Dialog.Title
                  as="h3"
                  className="text-lg font-bold text-gray-900 dark:text-white"
                >
                  <div>
                    {/* <BarsArrowDownIcon
                      className="h-6 w-6 text-indigo-600 dark:text-indigo-400"
                      aria-hidden="true"
                  /> */}
                  </div>
                  Workflow Rules
                </Dialog.Title>
                <hr className="my-4 border-gray-300 dark:border-gray-700" />
                <h2 className="text-med font-semibold text-gray-900 dark:text-white">
                  ➕ Set workflow rule
                </h2>
                <div className="mt-2 max-h-[40vh] min-w-[16rem] overflow-y-auto px-1 pb-1 md:min-w-[40rem] lg:max-h-[65vh] lg:min-w-[40rem]">
                  {/* <div className="mt-4">
                  <h3 class="mb-4 font-semibold text-gray-900 dark:text-white">Is this rule for the whole pipeline, or one job?</h3>
                  <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg sm:flex dark:bg-gray-700 dark:border-gray-600 dark:text-white">
                    <li className="w-full border-b border-gray-200 sm:border-b-0 sm:border-r dark:border-gray-600">
                      <div className="flex items-center pl-4"> 
                        <input 
                          id="workflow-rule-pipeline" 
                          type="radio" 
                          value="pipeline" 
                          name="workflow-rule" 
                          className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                          checked={ruleLevel === 'pipeline'} 
                          onChange={() =>  {
                            setRuleLevel('pipeline')
                            console.log(jobs)
                          }}
                        />
                        <label 
                          htmlFor="workflow-rule-pipeline" 
                          className="w-full py-3 px-3 ms-2 text-sm font-medium text-gray-900 dark:text-gray-300 inline-block"
                        >
                          Pipeline-level Rule
                        </label>
                      </div>
                    </li>
                    <li className="w-full dark:border-gray-600">
                      <div className="flex items-center pl-4">
                        <input 
                          id="workflow-rule-job" 
                          type="radio" 
                          value="job" 
                          name="workflow-rule" 
                          className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500" 
                          checked={ruleLevel === 'job'} 
                          onChange={() => setRuleLevel('job')} 
                        />
                        <label 
                          htmlFor="workflow-rule-job" 
                          className="w-full py-3 px-3 ms-2 text-sm font-medium text-gray-900 dark:text-gray-300 inline-block"
                        >
                          Job-level Rule
                        </label>
                      </div>
                    </li>
                  </ul>
                  </div> */}
                  <div className="mt-4">
                    <label
                      htmlFor="requestID"
                      className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Enter a new workflow rule, or select from an example
                      preset below:
                    </label>
                    <textarea
                      id="dynamicTextarea"
                      className="relative mt-2 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                      rows={calculateRows(newWorkflow)} // Set the number of rows dynamically
                      spellCheck="false"
                      value={newWorkflow}
                      onChange={handleWorkflowChange}
                    />
                  </div>
                  {ruleLevel === 'job' && (
                    <div className="mt-4">
                      <label
                        htmlFor="jobSelection"
                        className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                      >
                        Select Job where rule applies:
                      </label>
                      <select
                        id="jobSelection"
                        className="mt-1 block w-full appearance-none rounded-md border border-gray-300 bg-white px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                        value={selectedJob}
                        onChange={(e) => setSelectedJob(e.target.value)}
                      >
                        <option value="">Select Job</option>
                        {jobs.map((job) => (
                          <option key={job} value={job}>
                            {job}
                          </option>
                        ))}
                      </select>
                    </div>
                  )}
                  <div className="mt-2">
                    <label
                      htmlFor="presetRules"
                      className="block py-3 text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Preset rules:
                    </label>
                    <div className="grid grid-cols-2 gap-2">
                      {presetRules.map((preset, index) => (
                        <button
                          key={index}
                          type="button"
                          className="inline-flex text-sm w-full justify-center rounded-md bg-blue-600 py-1 text-white hover:bg-blue-800 focus:outline-none focus:ring-2 focus:ring-blue-800 focus:ring-offset-2"
                          onClick={() => setNewWorkflow(preset.script)}
                        >
                          {preset.title}
                        </button>
                      ))}
                    </div>
                  </div>

                  <hr className="my-4 border-gray-300 dark:border-gray-700" />
                </div>

                <div className="sm:flex sm:flex-row-reverse">
                  <button
                    type="submit"
                    id="submitButton"
                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-4 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    onClick={handleSubmit}
                  >
                    Set Workflow Rule
                  </button>
                  <button
                    type="button"
                    className="ml-3 mt-3 inline-flex w-full justify-center rounded-md bg-slate-100 px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-slate-200 dark:bg-slate-800 dark:text-white dark:ring-gray-600 dark:hover:bg-slate-900 sm:mt-0 sm:w-auto"
                    onClick={onClose}
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
