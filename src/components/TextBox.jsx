import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { solarizedlight } from 'react-syntax-highlighter/dist/esm/styles/prism'

const CodeHighlighter = ({ codeString }) => (
  <SyntaxHighlighter language="javascript" style={solarizedlight}>
    {codeString}
  </SyntaxHighlighter>
)

export default CodeHighlighter
