import { Fragment, useEffect, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { BarsArrowDownIcon, XMarkIcon } from '@heroicons/react/24/outline'
import { yamlToArray, arrayToYaml, addStage } from '@/helpers/Converters'
import { yaml } from '@codemirror/lang-yaml'
import { useCodeMirror } from '@/components/CodeMirrorContext'
import clsx from 'clsx'
import Highlight, { defaultProps } from 'prism-react-renderer'
import APIHelper from '@/helpers/APIHelper'

export function AdjustWithAIModal({ open, onClose, yamlData }) {
  const { code, updateCode } = useCodeMirror()
  const codeLanguage = 'javascript'

  const [aiResponse, setAiResponse] = useState(null)

  const [responseReady, setResponseReady] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [isQueryOpen, setIsQueryOpen] = useState(true)
  const [isResponseOpen, setIsResponseOpen] = useState(false)

  const askAI = async () => {
    // Begin loading state
    setIsLoading(true)
    try {
      let additionalQuery = document.getElementById('additionalQuery')
      let additionalQueryValue = additionalQuery
        ? additionalQuery.value.trim()
        : ''

      let queryInput =
        'adjust the following YAML file per my instructions and return the YAML file enclosed in ``` blocks:\n\n' +
        code +
        '\n\n' +
        additionalQueryValue +
        "\n please don't forget the --- at the start"

      const response = await APIHelper(queryInput)
      console.log(response)
      setAiResponse(response)
      setResponseReady(true)
      setIsResponseOpen(true)
    } catch (error) {
      console.error('Error:', error)
    }
    // End loading state
    setIsLoading(false)
  }

  const clear = () => {
    setResponseReady(false)
    setIsQueryOpen(true)
    setIsResponseOpen(false)
    setAiResponse(null)
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 z-50 overflow-y-auto scrollbar-thin scrollbar-track-slate-300 scrollbar-thumb-indigo-400 dark:bg-opacity-75 dark:scrollbar-track-slate-700 dark:scrollbar-thumb-indigo-300"
        onClose={onClose}
      >
        <div className="flex min-h-screen items-center justify-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-600 bg-opacity-50 backdrop-blur-sm backdrop-filter dark:bg-gray-900 dark:bg-opacity-50" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div>
              <div className="relative mx-1 mx-auto mt-6 max-w-screen-2xl overflow-y-auto rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl scrollbar-thin scrollbar-track-slate-300 scrollbar-thumb-indigo-400 dark:bg-gray-800 dark:scrollbar-track-slate-700 dark:scrollbar-thumb-indigo-300 sm:my-10 sm:w-full sm:max-w-3xl sm:p-6 lg:max-h-[90vh]">
                <div className="absolute right-0 top-0 hidden pr-4 pt-4 sm:block">
                  <button
                    type="button"
                    className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:bg-gray-800 dark:text-gray-600 dark:hover:text-gray-400"
                    onClick={onClose}
                  >
                    <span className="sr-only">Close</span>
                    <XMarkIcon
                      className="h-6 w-6 text-indigo-500 hover:text-indigo-600 dark:text-indigo-300 dark:hover:text-indigo-400"
                      aria-hidden="true"
                    />
                  </button>
                </div>
                <Dialog.Title
                  as="h3"
                  className="text-lg font-bold text-gray-900 dark:text-white"
                >
                  <div></div>
                  Adjust with AI
                </Dialog.Title>
                <hr className="my-4 border-gray-300 dark:border-gray-700" />
                <h2 className="text-med font-semibold text-gray-900 dark:text-white">
                  AI Query
                  <button
                    type="submit"
                    id="submitButton"
                    className="ml-2 mb-0 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-700 focus:ring-offset-2"
                    onClick={() =>
                      isQueryOpen ? setIsQueryOpen(false) : setIsQueryOpen(true)
                    }
                  >
                    {isQueryOpen ? 'hide' : 'show'}
                  </button>
                </h2>

                {isQueryOpen && (
                  <div
                    className={`mt-2 ${
                      isResponseOpen ? 'max-h-[30vh]' : 'max-h-[60vh]'
                    } min-w-[16rem] overflow-y-auto px-1 pb-1 pr-3 scrollbar-thin scrollbar-track-slate-300 scrollbar-thumb-indigo-400 dark:scrollbar-track-slate-700 dark:scrollbar-thumb-indigo-300 md:min-w-[40rem] ${
                      isResponseOpen ? 'lg:max-h-[30vh]' : 'lg:max-h-[60vh]'
                    } lg:min-w-[40rem]`}
                  >
                    <div className="mt-4">
                      <label
                        htmlFor="requestID"
                        className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                      >
                        Your current file contents for convenience:
                      </label>
                      <div className="relative">
                        <div className="absolute inset-0 rounded-2xl bg-gradient-to-tr from-sky-300 via-sky-300/70 to-blue-300 opacity-10 blur-lg" />
                        <div className="absolute inset-0 rounded-2xl bg-gradient-to-tr from-sky-300 via-sky-300/70 to-blue-300 opacity-10" />
                        <div className="relative rounded-2xl bg-[#0A101F]/80 ring-1 ring-white/10 backdrop-blur">
                          <div className="absolute -top-px left-20 right-11 h-px bg-gradient-to-r from-sky-300/0 via-sky-300/70 to-sky-300/0" />
                          <div className="absolute -bottom-px left-11 right-20 h-px bg-gradient-to-r from-blue-400/0 via-blue-400 to-blue-400/0" />
                          <div className="mt-4 pl-4 pt-0.5">
                            <div className="mt-6 flex items-start px-1 text-sm">
                              <div
                                aria-hidden="true"
                                className="select-none border-r border-slate-300/5 pr-4 font-mono text-slate-400 dark:text-slate-600"
                              >
                                {Array.from({
                                  length: code.split('\n').length,
                                }).map((_, index) => (
                                  <Fragment key={index}>
                                    {(index + 1).toString().padStart(2, '0')}
                                    <br />
                                  </Fragment>
                                ))}
                              </div>
                              <Highlight
                                {...defaultProps}
                                code={code}
                                language={codeLanguage}
                                theme={undefined}
                              >
                                {({
                                  className,
                                  style,
                                  tokens,
                                  getLineProps,
                                  getTokenProps,
                                }) => (
                                  <pre
                                    className={clsx(
                                      className,
                                      'flex overflow-x-auto pb-6 scrollbar-thin scrollbar-track-slate-300 scrollbar-thumb-indigo-400 dark:scrollbar-track-slate-700 dark:scrollbar-thumb-indigo-300'
                                    )}
                                    style={style}
                                  >
                                    <code className="px-4">
                                      {tokens.map((line, lineIndex) => (
                                        <div
                                          key={lineIndex}
                                          {...getLineProps({ line })}
                                        >
                                          {line.map((token, tokenIndex) => (
                                            <span
                                              key={tokenIndex}
                                              {...getTokenProps({ token })}
                                            />
                                          ))}
                                        </div>
                                      ))}
                                    </code>
                                  </pre>
                                )}
                              </Highlight>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mt-4">
                      <label
                        htmlFor="stageLocation"
                        className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                      >
                        Please instruct the AI on how you would like your file
                        adjusted: (e.g. "set the default image to
                        debian:trixie")
                      </label>
                      <textarea
                        id="additionalQuery"
                        name="additionalQuery"
                        className="relative mt-4 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                        rows={2}
                        spellCheck="false"
                        defaultValue={''}
                      />
                    </div>
                    <button
                      type="submit"
                      id="submitButton"
                      className="mt-2 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-4 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                      onClick={askAI}
                      disabled={isLoading}
                    >
                      {isLoading ? (
                        <svg
                          class="-ml-1 mr-3 h-5 w-5 animate-spin text-white"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                        >
                          <circle
                            class="opacity-25"
                            cx="12"
                            cy="12"
                            r="10"
                            stroke="currentColor"
                            stroke-width="4"
                          ></circle>
                          <path
                            class="opacity-75"
                            fill="currentColor"
                            d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                          ></path>
                        </svg>
                      ) : null}
                      {isLoading ? 'Processing...' : 'Ask AI'}
                    </button>
                  </div>
                )}

                <hr className="my-4 border-gray-300 dark:border-gray-700" />

                <h2 className="text-med font-semibold text-gray-900 dark:text-white">
                  AI Response{' '}
                  <span className="text-indigo-800 dark:text-indigo-300">
                    {!responseReady &&
                      ' - response will appear here when ready'}
                  </span>
                  {responseReady && (
                    <button
                      type="submit"
                      id="submitButton"
                      className="ml-2 mb-0 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-700 focus:ring-offset-2"
                      onClick={() =>
                        isResponseOpen
                          ? setIsResponseOpen(false)
                          : setIsResponseOpen(true)
                      }
                    >
                      {isResponseOpen ? 'hide' : 'show'}
                    </button>
                  )}
                </h2>
                {isResponseOpen && (
                  <div
                    className={`mt-2 ${
                      isQueryOpen ? 'max-h-[30vh]' : 'max-h-[60vh]'
                    } min-w-[16rem] overflow-y-auto px-1 pb-1 pr-3 scrollbar-thin scrollbar-track-slate-300 scrollbar-thumb-indigo-400 dark:scrollbar-track-slate-700 dark:scrollbar-thumb-indigo-300 md:min-w-[40rem] ${
                      isQueryOpen ? 'lg:max-h-[30vh]' : 'lg:max-h-[60vh]'
                    } lg:min-w-[40rem]`}
                  >
                    <div className="mt-4">
                      <div>{aiResponse}</div>
                      <span className="opacity-0">
                        ----------------------------------------------------------------------------------------------
                      </span>
                    </div>
                  </div>
                )}
                <hr className="my-4 border-gray-300 dark:border-gray-700" />
                <div className="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
                  <button
                    type="button"
                    className="ml-3 mt-3 inline-flex w-full justify-center rounded-md bg-slate-100 px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-slate-200 dark:bg-slate-800 dark:text-white dark:ring-gray-600 dark:hover:bg-slate-900 sm:mt-0 sm:w-auto"
                    onClick={onClose}
                  >
                    Close
                  </button>
                  <button
                    type="button"
                    className="ml-3 mt-3 inline-flex w-full justify-center rounded-md bg-slate-100 px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-slate-200 dark:bg-slate-800 dark:text-white dark:ring-gray-600 dark:hover:bg-slate-900 sm:mt-0 sm:w-auto"
                    onClick={clear}
                  >
                    Clear
                  </button>
                  <button
                    type="submit"
                    id="submitButton"
                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-4 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    onClick={askAI}
                    disabled={isLoading}
                  >
                    {isLoading ? (
                      <svg
                        class="-ml-1 mr-3 h-5 w-5 animate-spin text-white"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                      >
                        <circle
                          class="opacity-25"
                          cx="12"
                          cy="12"
                          r="10"
                          stroke="currentColor"
                          stroke-width="4"
                        ></circle>
                        <path
                          class="opacity-75"
                          fill="currentColor"
                          d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                        ></path>
                      </svg>
                    ) : null}
                    {isLoading ? 'Processing...' : 'Ask AI'}
                  </button>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
