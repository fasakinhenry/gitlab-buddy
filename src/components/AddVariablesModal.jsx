import { Fragment, useEffect, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { BarsArrowDownIcon, XMarkIcon } from '@heroicons/react/24/outline'
import {
  yamlToArray,
  arrayToYaml,
  addStage,
  addVariable,
} from '@/helpers/Converters'
import { yaml } from '@codemirror/lang-yaml'
import { useCodeMirror } from '@/components/CodeMirrorContext'

export function AddVariablesModal({ open, onClose, yamlData }) {
  const { code, updateCode } = useCodeMirror()
  const [parsedYaml, setParsedYaml] = useState(yamlToArray(code))
  const [newVariable, setNewVariable] = useState('NEW_VARIABLE')
  const [newVariableValue, setNewVariableValue] = useState('')

  useEffect(() => {
    // Parse the YAML data and extract the stages
    const parsedYamlData = yamlToArray(yamlData)

    // Set parsed YAML data state
    setParsedYaml(parsedYamlData)
  }, [yamlData])

  useEffect(() => {
    console.log(newVariable)
  }, [newVariable])

  const handleVariableChange = (event) => {
    setNewVariable(event.target.value)
  }

  const handleVariableValueChange = (event) => {
    setNewVariableValue(event.target.value)
  }

  const handleSubmit = () => {
    console.log(`newVariable: ${newVariable}`)
    console.log(`newVariableValue: ${newVariableValue}`)
    addVariable(parsedYaml, newVariable, newVariableValue)
    console.log(parsedYaml)
    const parsedYamlData = arrayToYaml(parsedYaml)

    console.log(parsedYamlData)
    updateCode(parsedYamlData)
    onClose()
  }

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 z-50 overflow-y-auto dark:bg-opacity-75"
        onClose={onClose}
      >
        <div className="flex min-h-screen items-center justify-center">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-600 bg-opacity-50 backdrop-blur-sm backdrop-filter dark:bg-gray-900 dark:bg-opacity-50" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="ease-out duration-500"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-300"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div>
              <div className="relative mx-auto mt-6 max-w-screen-2xl rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl dark:bg-gray-800 sm:my-20 sm:w-full sm:max-w-3xl sm:p-6">
                <div className="absolute right-0 top-0 hidden pr-4 pt-4 sm:block">
                  <button
                    type="button"
                    className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:bg-gray-800 dark:text-gray-600 dark:hover:text-gray-400"
                    onClick={onClose}
                  >
                    <span className="sr-only">Close</span>
                    <XMarkIcon
                      className="h-6 w-6 text-indigo-500 hover:text-indigo-600 dark:text-indigo-300 dark:hover:text-indigo-400"
                      aria-hidden="true"
                    />
                  </button>
                </div>
                <Dialog.Title
                  as="h3"
                  className="text-lg font-bold text-gray-900 dark:text-white"
                >
                  <div>
                    {/* <BarsArrowDownIcon
                      className="h-6 w-6 text-indigo-600 dark:text-indigo-400"
                      aria-hidden="true"
                  /> */}
                  </div>
                  Variables
                </Dialog.Title>
                <hr className="my-4 border-gray-300 dark:border-gray-700" />
                <h2 className="text-med font-semibold text-gray-900 dark:text-white">
                  ➕ Add a variable
                </h2>
                <div className="mt-2 max-h-[40vh] min-w-[16rem] overflow-y-auto px-1 pb-1 md:min-w-[40rem] lg:max-h-[65vh] lg:min-w-[40rem]">
                  <div className="mt-4">
                    <label
                      htmlFor="requestID"
                      className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Variable name:
                    </label>
                    <textarea
                      id="newVariableName"
                      name="newVariableName"
                      className="relative mt-1 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                      rows={1}
                      spellCheck="false"
                      value={newVariable}
                      onChange={handleVariableChange}
                    />
                  </div>
                  <div className="mt-4">
                    <label
                      htmlFor="requestID"
                      className="block text-sm font-medium leading-5 text-gray-900 dark:text-white"
                    >
                      Variable value:
                    </label>
                    <textarea
                      id="newVariableValue"
                      name="newVariableValue"
                      className="relative mt-1 block w-full appearance-none rounded-md border border-gray-300 bg-slate-100 px-3 py-2 font-mono text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 dark:border-gray-600 dark:border-gray-700 dark:bg-slate-700 dark:text-white dark:placeholder-gray-300 dark:focus:border-indigo-500 sm:text-sm"
                      rows={1}
                      spellCheck="false"
                      value={newVariableValue}
                      onChange={handleVariableValueChange}
                    />
                  </div>
                  <div className="mt-4">
                    <label className="block text-sm font-light leading-5 text-gray-900 dark:text-white">
                      You can use this variable in the pipeline like so:{' '}
                      <span className="bg-slate-200 font-mono text-indigo-500 dark:bg-slate-700 dark:text-indigo-200">
                        ${'{'}
                        {newVariable}
                        {'}'}
                      </span>
                    </label>
                  </div>

                  <hr className="my-4 border-gray-300 dark:border-gray-700" />
                </div>

                <div className="sm:flex sm:flex-row-reverse">
                  <button
                    type="submit"
                    id="submitButton"
                    className="ml-3 inline-flex justify-center rounded-md border border-transparent bg-indigo-500 px-4 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    onClick={handleSubmit}
                  >
                    Add Variable
                  </button>
                  <button
                    type="button"
                    className="ml-3 mt-3 inline-flex w-full justify-center rounded-md bg-slate-100 px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-slate-200 dark:bg-slate-800 dark:text-white dark:ring-gray-600 dark:hover:bg-slate-900 sm:mt-0 sm:w-auto"
                    onClick={onClose}
                  >
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
