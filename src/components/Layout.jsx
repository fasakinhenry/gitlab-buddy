import { useCallback, useEffect, useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import clsx from 'clsx'
import { Hero } from '@/components/Hero'
import { Logo, Logomark } from '@/components/Logo'
import { MobileNavigation } from '@/components/MobileNavigation'
import { Navigation } from '@/components/Navigation'
import { Prose } from '@/components/Prose'
import { Search } from '@/components/Search'
import { ThemeSelector } from '@/components/ThemeSelector'
import { useCodeMirror } from './CodeMirrorContext';
import { ExplainWithAIModal } from '@/components/ExplainWithAIModal'
import { AdjustWithAIModal } from './AdjustWithAIModal'
import { FixWithAIModal } from './FixWithAIModal'

const navigation = [
  {
    title: 'Use GitLab Buddy',
    links: [
      { title: 'GitLab Buddy App', href: '/#gitlab-buddy' },
    ],
  },
  {
    title: 'About GitLab',
    links: [
      { title: 'What is GitLab?', href: '/docs/what-is-gitlab' },
      {
        title: 'What is a GitLab Pipeline?', href: '/docs/what-is-a-gitlab-pipeline'},
      { title: 'What is a gitlab-ci.yml File?', href: '/docs/gitlab-ci-yml' },
      {
        title: 'YAML Formatting and Rules',
        href: '/docs/yaml-formatting-and-rules',
      },
    ],
  },
  {
    title: 'About GitLab Buddy',
    links: [
      { title: 'What is GitLab Buddy?', href: '/docs/gitlab-buddy' },
      {
        title: 'How to Use GitLab Buddy',
        href: '/docs/how-to-use-gitlab-buddy',
      },
      {
        title: 'Future Plans',
        href: '/docs/future-plans',
      },
    ],
  },
]

function GitLabIcon(props) {
  return (
    <svg aria-hidden="true" viewBox="0 0 16 16" {...props}>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><rect width="512" height="512" rx="15%" fill="#30353e"/><path fill="#e24329" d="M84 215l43-133c2-7 12-7 14 0l115 353L371 82c2-7 12-7 14 0l43 133"/><path fill="#fc6d26" d="M256 435L84 215h100.4zm71.7-220H428L256 435l71.6-220z"/><path fill="#fca326" d="M84 215l-22 67c-2 6 0 13 6 16l188 137zm344 0l22 67c2 6 0 13-6 16L256 435z"/></svg>
    </svg>
  )
}

function Header({ navigation }) {
  let [isScrolled, setIsScrolled] = useState(false)

  useEffect(() => {
    function onScroll() {
      setIsScrolled(window.scrollY > 0)
    }
    onScroll()
    window.addEventListener('scroll', onScroll, { passive: true })
    return () => {
      window.removeEventListener('scroll', onScroll, { passive: true })
    }
  }, [])

  return (
    <header
      className={clsx(
        'sticky top-0 z-50 flex flex-wrap items-center justify-between bg-white px-4 py-5 shadow-md shadow-slate-900/5 transition duration-500 dark:shadow-none sm:px-6 lg:px-8',
        isScrolled
          ? 'dark:bg-slate-900/95 dark:backdrop-blur dark:[@supports(backdrop-filter:blur(0))]:bg-slate-900/75'
          : 'dark:bg-transparent'
      )}
    >
      <div className="mr-6 flex lg:hidden">
        <MobileNavigation navigation={navigation} />
      </div>
      <div className="relative flex flex-grow basis-0 items-center">
        <Link href="/" aria-label="Home page">
          <Logomark className="h-9 w-9 lg:hidden" />
          <Logo className="hidden h-9 w-auto fill-slate-700 dark:fill-sky-100 lg:block" />
        </Link>
      </div>
      {/* <div className="-my-5 mr-6 sm:mr-8 md:mr-0">
        <Search />
      </div> */}
      <div className="relative flex basis-0 justify-end gap-6 sm:gap-8 md:flex-grow">
        <ThemeSelector className="relative z-10" />
        <Link href="https://gitlab.com" className="group" aria-label="GitHub">
          <GitLabIcon className="h-8 w-8 fill-slate-400 group-hover:fill-slate-500 dark:group-hover:fill-slate-300" />
        </Link>
      </div>
    </header>
  )
}

function useTableOfContents(tableOfContents) {
  let [currentSection, setCurrentSection] = useState(tableOfContents[0]?.id)

  let getHeadings = useCallback((tableOfContents) => {
    return tableOfContents
      .flatMap((node) => [node.id, ...node.children.map((child) => child.id)])
      .map((id) => {
        let el = document.getElementById(id)
        if (!el) return

        let style = window.getComputedStyle(el)
        let scrollMt = parseFloat(style.scrollMarginTop)

        let top = window.scrollY + el.getBoundingClientRect().top - scrollMt
        return { id, top }
      })
  }, [])

  useEffect(() => {
    if (tableOfContents.length === 0) return
    let headings = getHeadings(tableOfContents)
    function onScroll() {
      let top = window.scrollY
      let current = headings[0].id
      for (let heading of headings) {
        if (top >= heading.top) {
          current = heading.id
        } else {
          break
        }
      }
      setCurrentSection(current)
    }
    window.addEventListener('scroll', onScroll, { passive: true })
    onScroll()
    return () => {
      window.removeEventListener('scroll', onScroll, { passive: true })
    }
  }, [getHeadings, tableOfContents])

  return currentSection
}

export function Layout({ children, title, tableOfContents }) {
  let router = useRouter()
  let isHomePage = router.pathname === '/'
  let previousPage, nextPage;
  if (router.pathname === '/') {
    // For the first page, set the previous page to null
    previousPage = null;
    
    // Find the index of the GitLab Buddy link in the navigation array
    const gitlabBuddyIndex = navigation.findIndex(section => section.links.some(link => link.href === '/#gitlab-buddy'));
    
    // Calculate the index of the next page based on the GitLab Buddy link
    nextPage = navigation[gitlabBuddyIndex + 1]?.links[0] || null;
  } else {
    // For other pages, use the original logic to find previous and next pages
    const allLinks = navigation.flatMap(section => section.links);
    const linkIndex = allLinks.findIndex(link => link.href === router.pathname);
    previousPage = allLinks[linkIndex - 1] || null;
    nextPage = allLinks[linkIndex + 1] || null;
  }
  let section = navigation.find((section) =>
    section.links.find((link) => link.href === router.pathname)
  )
  let currentSection = useTableOfContents(tableOfContents)

  function isActive(section) {
    if (section.id === currentSection) {
      return true
    }
    if (!section.children) {
      return false
    }
    return section.children.findIndex(isActive) > -1
  }

  const { code, updateCode, error } = useCodeMirror();
  const [selectedComponent, setSelectedComponent] = useState(null)
  const openExplainWithAIModal = () => {setSelectedComponent("explain-ai");}
  const openAdjustWithAIModal = () => {setSelectedComponent("adjust-ai");}
  const openFixWithAIModal = () => {setSelectedComponent("fix-ai");}

  const copyToClipboard = () => {
    navigator.clipboard.writeText(code)
      .then(() => {
        console.log('Code copied to clipboard successfully');
      })
      .catch((error) => {
        console.error('Failed to copy code to clipboard:', error);
      });
  };

  const downloadFile = () => {
    const blob = new Blob([code], { type: 'text/plain' });
    const anchor = document.createElement('a');
    anchor.href = URL.createObjectURL(blob);
    anchor.download = '.gitlab-ci.yml';
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  };

  return (
    <>
      <Header navigation={navigation} />

      {isHomePage && <Hero />}

      <div className="relative mx-auto flex max-w-8xl justify-center sm:px-2 lg:px-8 xl:px-12">
        <div className="hidden lg:relative lg:block lg:flex-none">
          <div className="absolute inset-y-0 right-0 w-[50vw] bg-slate-50 dark:hidden" />
          <div className="sticky top-[4.5rem] -ml-0.5 h-[calc(100vh-4.5rem)] overflow-y-auto py-16 pl-0.5">
            <div className="absolute top-16 bottom-0 right-0 hidden h-12 w-px bg-gradient-to-t from-slate-800 dark:block" />
            <div className="absolute top-28 bottom-0 right-0 hidden w-px bg-slate-800 dark:block" />
            <Navigation
              navigation={navigation}
              className="w-64 pr-8 xl:w-72 xl:pr-16"
            />
          </div>
        </div>
        <div className="min-w-0 max-w-2xl flex-auto px-4 py-16 lg:max-w-none lg:pr-0 lg:pl-8 xl:px-16">
          <article>
            {(title || section) && (
              <header className="mb-9 space-y-1">
                {section && (
                  <p className="font-display font-medium text-sky-500">
                    {section.title}
                  </p>
                )}
                {title && (
                  <h1 className="font-display text-3xl tracking-tight text-slate-900 dark:text-white">
                    {title}
                  </h1>
                )}
              </header>
            )}
            <Prose>{children}</Prose>
          </article>
          <dl className="mt-12 flex border-t border-slate-200 pt-6 dark:border-slate-800">
            {previousPage && (
              <div>
                <dt className="font-display text-sm font-medium text-slate-900 dark:text-white">
                  Previous
                </dt>
                <dd className="mt-1">
                  <Link
                    href={previousPage.href}
                    className="text-base font-semibold text-slate-500 hover:text-slate-600 dark:text-slate-400 dark:hover:text-slate-300"
                  >
                    <span aria-hidden="true">&larr;</span> {previousPage.title}
                  </Link>
                </dd>
              </div>
            )}
            {nextPage && (
              <div className="ml-auto text-right">
                <dt className="font-display text-sm font-medium text-slate-900 dark:text-white">
                  Next
                </dt>
                <dd className="mt-1">
                  <Link
                    href={nextPage.href}
                    className="text-base font-semibold text-slate-500 hover:text-slate-600 dark:text-slate-400 dark:hover:text-slate-300"
                  >
                    {nextPage.title} <span aria-hidden="true">&rarr;</span>
                  </Link>
                </dd>
              </div>
            )}
          </dl>
        </div>
        <div className="hidden xl:sticky xl:top-[4.5rem] xl:-mr-6 xl:block xl:h-[calc(100vh-4.5rem)] xl:flex-none xl:overflow-y-auto xl:py-16 xl:pr-6">
          <nav aria-labelledby="on-this-page-title" className="w-56">
            {tableOfContents.length >= 0 && (
              <>
                {isHomePage && 
                  <>
                    <button
                      type="button"
                      onClick={copyToClipboard}
                      className="mt-20 mb-2 ml-1 w-full justify-center text-center font-bold inline-flex items-center rounded-md border border-transparent bg-blue-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-800 focus:ring-offset-2"
                    >
                      Copy to Clipboard
                    </button>
                    <button
                      type="button"
                      onClick={downloadFile}
                      className="my-2 ml-1 w-full justify-center text-center font-bold inline-flex items-center rounded-md border border-transparent bg-blue-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-800 focus:ring-offset-2"
                    >
                      Download File
                    </button>
                    <button
                      type="button"
                      className="my-2 ml-1 w-full justify-center text-center font-bold inline-flex items-center rounded-md border border-transparent bg-sky-400 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-sky-800 focus:ring-offset-2"
                      onClick={openExplainWithAIModal}
                    >
                      Explain with AI
                    </button>
                    <button
                      type="button"
                      className="my-2 ml-1 w-full justify-center text-center font-bold inline-flex items-center rounded-md border border-transparent bg-sky-400 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-sky-800 focus:ring-offset-2"
                      onClick={openAdjustWithAIModal}
                    >
                      Adjust with AI 
                    </button>
                    {error && (
                      <button
                        type="button"
                        className="my-2 ml-1 w-full justify-center text-center font-bold inline-flex items-center rounded-md border border-transparent bg-emerald-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-emerald-700 focus:outline-none focus:ring-2 focus:ring-emerald-800 focus:ring-offset-2"
                        onClick={openFixWithAIModal}
                      >
                        Fix with AI 
                      </button>
                    )}

                    <ExplainWithAIModal
                      open={selectedComponent === 'explain-ai'}
                      props=''
                      yamlData={code}
                      options=''
                      onClose={(error) => {
                        setSelectedComponent(null)
                      }}
                    />

                    <AdjustWithAIModal
                      open={selectedComponent === 'adjust-ai'}
                      props=''
                      yamlData={code}
                      options=''
                      onClose={(error) => {
                        setSelectedComponent(null)
                      }}
                    />

                    <FixWithAIModal
                      open={selectedComponent === 'fix-ai'}
                      props=''
                      yamlData={code}
                      options=''
                      onClose={(error) => {
                        setSelectedComponent(null)
                      }}
                    />
                  </>
                }
                <ol role="list" className="mt-4 space-y-3 text-sm">
                  {tableOfContents.map((section) => (
                    <li key={section.id}>
                      <h3>
                        <Link
                          href={`#${section.id}`}
                          className={clsx(
                            isActive(section)
                              ? 'text-sky-500'
                              : 'font-normal text-slate-500 hover:text-slate-700 dark:text-slate-400 dark:hover:text-slate-300'
                          )}
                        >
                          {section.title}
                        </Link>
                      </h3>
                      {section.children.length > 0 && (
                        <ol
                          role="list"
                          className="mt-2 space-y-3 pl-5 text-slate-500 dark:text-slate-400"
                        >
                          {section.children.map((subSection) => (
                            <li key={subSection.id}>
                              <Link
                                href={`#${subSection.id}`}
                                className={
                                  isActive(subSection)
                                    ? 'text-sky-500'
                                    : 'hover:text-slate-600 dark:hover:text-slate-300'
                                }
                              >
                                {subSection.title}
                              </Link>
                            </li>
                          ))}
                        </ol>
                      )}
                    </li>
                  ))}
                </ol>
              </>
            )}
          </nav>
        </div>
      </div>
    </>
  )
}
