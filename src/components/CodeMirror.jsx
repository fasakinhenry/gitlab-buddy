import { Fragment, useRef, useState, useEffect } from 'react'
import {
  autocompletion,
  closeBrackets,
  closeBracketsKeymap,
  completionKeymap,
} from '@codemirror/autocomplete'
import { defaultKeymap, history, historyKeymap } from '@codemirror/commands'
import {
  bracketMatching,
  defaultHighlightStyle,
  foldGutter,
  foldKeymap,
  indentOnInput,
  syntaxHighlighting,
} from '@codemirror/language'
import { lintKeymap } from '@codemirror/lint'
import { highlightSelectionMatches, searchKeymap } from '@codemirror/search'
import { EditorState } from '@codemirror/state'
import {
  EditorView,
  crosshairCursor,
  drawSelection,
  dropCursor,
  highlightActiveLine,
  highlightActiveLineGutter,
  highlightSpecialChars,
  keymap,
  lineNumbers,
  rectangularSelection,
} from '@codemirror/view'
import { yaml } from '@codemirror/lang-yaml'
import { tokyoNight } from '@uiw/codemirror-theme-tokyo-night'


export function CodeMirror({ open, code, updateCode }) {
  const editorContainerRef = useRef(null);
  const editorViewRef = useRef(null);

    
  const borderTheme = EditorView.theme({
    '.cm-editor': { 
      'border-radius': '0.375rem',
    },
    '&': {
      'border-radius': '0.375rem',
    },
    '.cm-content': {
      'background': 'linear-gradient(to left, rgba(0, 255, 255, 0.05), rgba(0, 191, 255, 0.05), rgba(15, 23, 42, 0.7)) !important', // Cyan sky slate gradient background with more transparency
    },
    '.cm-scroller': {
      'border-radius': '0.375rem',
      'background-color': 'rgba(8, 23, 42, 0.9)',
    },
    '.cm-focused': { 'border-radius': '0.375rem' },
    '.cm-gutters': {
      'padding-left': '0.5em',
      'padding-right': '0.2em',
      'background-color': 'rgba(15, 23, 42, 0.8) !important', // Adjust the alpha channel as needed
      'border-right': '1.5px solid rgba(128, 128, 128, 0.3) !important', // Translucent gray border between line numbers and editor content
      'margin-right': '3px',
    },
    '.cm-gutter': {
      'color': 'rgba(100, 116, 139, 0.7) !important', // Organic color for line numbers
    },
  });

  useEffect(() => {

    if (!editorViewRef.current) {
      const extensions = [
        lineNumbers(),
        highlightActiveLineGutter(),
        highlightSpecialChars(),
        history(),
        foldGutter(),
        drawSelection(),
        dropCursor(),
        EditorState.allowMultipleSelections.of(true),
        indentOnInput(),
        syntaxHighlighting(defaultHighlightStyle, { fallback: true }),
        bracketMatching(),
        closeBrackets(),
        autocompletion(),
        rectangularSelection(),
        crosshairCursor(),
        highlightActiveLine(),
        highlightSelectionMatches(),
        keymap.of([
          ...closeBracketsKeymap,
          ...defaultKeymap,
          ...searchKeymap,
          ...historyKeymap,
          ...foldKeymap,
          ...completionKeymap,
          ...lintKeymap,
        ]),
        yaml(),
        tokyoNight,
        borderTheme,
      ];

      const newState = EditorState.create({
        doc: code,
        extensions,
      });

      const view = new EditorView({
        state: newState,
        parent: editorContainerRef.current,
        dispatch: (tr) => {
          const newCode = tr.state.doc.toString();
          view.update([tr]);
          updateCode(newCode); 
        },
      });

      editorViewRef.current = view;
    } else if (editorViewRef.current) {
      const currentCode = editorViewRef.current.state.doc.toString();
      if (currentCode !== code) {
        const changes = [{ from: 0, to: currentCode.length, insert: code }];
        const transaction = editorViewRef.current.state.update({ changes });
        editorViewRef.current.dispatch(transaction);
      }
    }
  }, [code, updateCode]);

  return (
    <div className="mt-1 relative rounded-[0.375rem] focus-within:z-10 focus-within:border-sky-500 focus-within:outline-none focus-within:ring focus-within:ring-[1.25px] focus-within:ring-sky-500 dark:border-gray-600 dark:focus-within:ring-[1.75px]">
      <div className="absolute inset-0 rounded-[0.375rem] bg-gradient-to-tr from-sky-300 via-sky-300/70 to-blue-300 opacity-10 blur-lg" />
      <div className="absolute inset-0 rounded-[0.375rem] bg-gradient-to-tr from-sky-300 via-sky-300/70 to-blue-300 opacity-10" />
      <div className="relative rounded-[0.375rem] bg-[#0A101F]/80 ring-1 ring-white/10 backdrop-blur">
        <div className="absolute -top-px left-20 right-11 h-px bg-gradient-to-r from-sky-300/0 via-sky-300/70 to-sky-300/0" />
        <div className="absolute -bottom-px left-11 right-20 h-px bg-gradient-to-r from-blue-400/0 via-blue-400 to-blue-400/0" />
        <div ref={editorContainerRef} className="block w-full font-mono"></div>
      </div>
    </div>
  );
}