# using Python 3.9 layer from https://github.com/erenyasarkurt/OpenAI-AWS-Lambda-Layer
import os
import openai

# Configure OpenAI API
openai.api_key = os.getenv("OPENAI_API_KEY")

def lambda_handler(event, context):
    # Retrieve the query from the request body
    query = event.get("body")

    if not query:
        return {
            "statusCode": 400,
            "body": "Query is missing in the request body.",
            "headers": {
                "Access-Control-Allow-Origin": "*",  # Adjust as needed
                "Access-Control-Allow-Headers": "**",  # Adjust as needed
                "Access-Control-Allow-Methods": "ANY,OPTIONS,POST",  # Adjust as needed
            }
        }

    # Generate explanation using OpenAI API
    explanation = generate_explanation(query)

    return {
        "statusCode": 200,
        "body": explanation,
        "headers": {
            "Access-Control-Allow-Origin": "*",  # Adjust as needed
            "Access-Control-Allow-Headers": "*",  # Adjust as needed
            "Access-Control-Allow-Methods": "AOPTIONS,POST",  # Adjust as needed
        }
    }

def generate_explanation(query):
    # Use OpenAI API to generate explanation
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "system", "content": query}],
        max_tokens=4096,
        temperature=0.5,
        n=1,
        stop=None,
        logprobs=None
    )

    return response.choices[0].message
